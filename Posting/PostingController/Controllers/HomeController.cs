﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PostingService.Services;
using Posting.DataAccesLayer;
using Posting.Models;
using System.ComponentModel.DataAnnotations;

namespace PostingController.Controllers
{
	public class HomeController : Controller
	{
		private static PostingContext db = new PostingContext();
		private PostService postService = new PostService(db);
		private CommentService commentService = new CommentService(db);
		private ReactionService reactService = new ReactionService(db);

	
		public ActionResult Index()
		{
			var allPosts = postService.GetAllPosts();


			return View(allPosts);
		}

		public ActionResult About()
		{
			ViewBag.Message = "Your application description page.";

			return View();
		}

		public ActionResult Contact()
		{
			ViewBag.Message = "Your contact page.";

			return View();
		}

		
		public ActionResult NewComment(string newText, Post item)
		{
			var text = newText;

			var com = commentService.CreateComment(item.UserID, item.PostID, newText);

			commentService.Add(com);
			commentService.Save();

			return RedirectToAction("Index", "Home");
		}

		public ActionResult NewReaction(Post item, string emotion)
		{
			var newReaction = reactService.CreateReaction(emotion, item);

			reactService.Add(newReaction);
			reactService.Save();

			return RedirectToAction("Index", "Home");
		}

	}
}