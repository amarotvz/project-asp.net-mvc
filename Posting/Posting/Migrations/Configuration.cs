namespace Posting.Migrations
{
	using System;
	using System.Data.Entity;
	using System.Data.Entity.Migrations;
	using System.Linq;
	using Posting.Models;

	internal sealed class Configuration : DbMigrationsConfiguration<Posting.DataAccesLayer.PostingContext>
	{
		public Configuration()
		{
			AutomaticMigrationsEnabled = true;
		}

		protected override void Seed(Posting.DataAccesLayer.PostingContext context)
		{
			var user1 = new User { Name = "Patrick", Birth = DateTime.Parse("1995-06-26"), Description = "I like posting", Email = "patrick@gmail.com", Password = "patrick1995" };
			var user2 = new User { Name = "Thomas", Birth = DateTime.Parse("1980-01-29"), Description = "Supporting Posting", Email = "thomas@gmail.com", Password = "thomas1980" };
			var user3 = new User { Name = "Mark", Birth = DateTime.Parse("1989-12-26"), Description = "Happy reading posts", Email = "marta@gmail.com", Password = "Marta1989" };
			var user4 = new User { Name = "Rose", Birth = DateTime.Parse("1992-08-10"), Description = "Comment my post", Email = "rose@gmail.com", Password = "Rose1992" };

			var post1 = new Post { UserID = 1, Text = "I'm developing this application", Time = DateTime.Now, User = user1 };
			var post2 = new Post { UserID = 2, Text = "Today I loged in Posting for first time and this is very exciting. I want to make friends here! :)", Time = DateTime.Now, User = user2 };
			var post3 = new Post { UserID = 3, Text = "My dream is to read Posting everywhere I go. For that I'm thinking on helping to develop this place and make it grow.", Time = DateTime.Now, User = user3 };
			var post4 = new Post { UserID = 4, Text = "I hope that my pizza isn't burnt while I'm writing in Posting...", Time = DateTime.Now, User = user4 };


			var reaction1 = new Reaction { Emotion = Emotions.Love, Post = post1, User = user1, PostID = 1, UserID = 1, Time = DateTime.Now };
			var reaction2 = new Reaction { Emotion = Emotions.Suprise, Post = post1, User = user2, PostID = 1, UserID = 2, Time = DateTime.Now };
			var reaction3 = new Reaction { Emotion = Emotions.Angry, Post = post2, User = user3, PostID = 2, UserID = 3, Time = DateTime.Now };
			var reaction4 = new Reaction { Emotion = Emotions.Funny, Post = post2, User = user4, PostID = 2, UserID = 4, Time = DateTime.Now };
			var reaction5 = new Reaction { Emotion = Emotions.Like, Post = post2, User = user1, PostID = 2, UserID = 1, Time = DateTime.Now };
			var reaction6 = new Reaction { Emotion = Emotions.Love, Post = post4, User = user1, PostID = 1, UserID = 1, Time = DateTime.Now };
			var reaction7 = new Reaction { Emotion = Emotions.Love, Post = post4, User = user3, PostID = 4, UserID = 3, Time = DateTime.Now };
			var reaction8 = new Reaction { Emotion = Emotions.Love, Post = post4, User = user2, PostID = 4, UserID = 2, Time = DateTime.Now };


			var comment1 = new Comment { User = user1, Post = post1, PostID = 1, UserID = 1, Text = "developing...", Time = DateTime.Now };
			var comment2 = new Comment { User = user2, Post = post2, PostID = 3, UserID = 2, Text = "Good begining!", Time = DateTime.Now };
			var comment3 = new Comment { User = user3, Post = post3, PostID = 4, UserID = 3, Text = "I keep dreaming", Time = DateTime.Now };
			var comment4 = new Comment { User = user4, Post = post4, PostID = 2, UserID = 4, Text = "I love pizza", Time = DateTime.Now };
			var comment5 = new Comment { User = user1, Post = post4, PostID = 1, UserID = 1, Text = "Share the pizza with me !", Time = DateTime.Now };
			var comment6 = new Comment { User = user2, Post = post3, PostID = 3, UserID = 2, Text = "One day it will be true :D", Time = DateTime.Now };
			var comment7 = new Comment { User = user4, Post = post3, PostID = 3, UserID = 4, Text = "Keep dreaming hahaha", Time = DateTime.Now };
			var comment8 = new Comment { User = user1, Post = post3, PostID = 3, UserID = 1, Text = "I hope someone started with that", Time = DateTime.Now };

			context.Users.Add(user1);
			context.Users.Add(user2);
			context.Users.Add(user3);
			context.Users.Add(user4);

			context.Posts.Add(post1);
			context.Posts.Add(post2);
			context.Posts.Add(post3);
			context.Posts.Add(post4);

			context.Comments.Add(comment1);
			context.Comments.Add(comment2);
			context.Comments.Add(comment3);
			context.Comments.Add(comment4);
			context.Comments.Add(comment5);
			context.Comments.Add(comment6);
			context.Comments.Add(comment7);
			context.Comments.Add(comment8);


			context.Reactions.Add(reaction1);
			context.Reactions.Add(reaction2);
			context.Reactions.Add(reaction3);
			context.Reactions.Add(reaction4);
			context.Reactions.Add(reaction5);
			context.Reactions.Add(reaction6);
			context.Reactions.Add(reaction7);
			context.Reactions.Add(reaction8);


			context.SaveChanges();

		}
	}
}