﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Posting.Models
{
	public class Comment
	{
		public int PostID { get; set; }
		public int UserID { get; set; }
		public int CommentID { get; set; }
		public string Text { get; set; }
		public DateTime Time { get; set; }

		public virtual User User { get; set; }
		public virtual Post Post { get; set; }
		
	}
}