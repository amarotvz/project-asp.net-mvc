﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Posting.Models
{
	public class Reaction
	{
		public Emotions Emotion { get; set; }
		public int PostID { get; set; }
		public int UserID { get; set; }
		public int ReactionID { get; set; }
		public DateTime Time { get; set; }

		public virtual Post Post { get; set; }
		public virtual User User { get; set; }
	
	}
}