﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Posting.Models
{
	public class Post
	{
		public int UserID { get; set; }
		public int PostID { get; set; }
		public string Text { get; set; }
		public DateTime Time { get; set; }

		public virtual User User { get; set; }

		public virtual ICollection<Comment> Comments { get; set; }
		public virtual ICollection<Reaction> Reactions { get; set; }

	}
}