﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace Posting.Models
{
	public class User
	{	
		public int UserID { get; set; }
		[DataType(DataType.Text)]
		public string Name { get; set; }
		[DataType(DataType.EmailAddress)]
		public string Email { get; set; }
		[DataType(DataType.Password)]
		public string Password { get; set; }
		[DataType(DataType.Text)]
		public string Description { get; set; }
		[DataType(DataType.Date)]
		public DateTime Birth { get; set; }

		public virtual ICollection<Post> Posts { get; set; }
		public virtual ICollection<Comment> Comments { get; set; }
		public virtual ICollection<User> Friends { get; set; }

	}
}