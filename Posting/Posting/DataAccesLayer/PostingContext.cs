﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Posting.Models;

namespace Posting.DataAccesLayer
{
	public class PostingContext : DbContext
	{
		public PostingContext() : base("PostingContext")
		{
		}

		public DbSet<User> Users { get; set; }
		public DbSet<Post> Posts { get; set; }
		public DbSet<Comment> Comments { get; set; }
		public DbSet<Reaction> Reactions { get; set; }

	}
}