﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Posting.DataAccesLayer;
using Posting.Models;

namespace PostingService.Services
{
	public class ReactionService
	{
		private PostingContext db;

		public ReactionService(PostingContext db)
		{
			this.db = db;
		}

		public void Add(Reaction reaction)
		{
			db.Reactions.Add(reaction);
		}

		public Reaction CreateReaction(string emotion, Post post)
		{
			var owner = db.Users.Where(p => p.UserID == 2).FirstOrDefault();
			var time = DateTime.Now;
			var emotionEnum = Emotions.Like;//(Emotions)Enum.Parse(typeof(Emotions), emotion);

			var reaction = new Reaction { Emotion = emotionEnum, PostID = post.PostID, UserID = owner.UserID, Time = time, Post = post, User = owner };

			return reaction;
		}

		public void Upate(Reaction reaction)
		{
		}

		public void Delete(Reaction reaction)
		{
			db.Reactions.Remove(reaction);
		}

		public void Save()
		{
			db.SaveChanges();
		}
	}
}