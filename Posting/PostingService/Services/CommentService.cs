﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Posting.DataAccesLayer;
using Posting.Models;
using PostingService.Services;

namespace PostingService.Services
{
	public class CommentService
	{
		private PostingContext db;

		public CommentService(PostingContext db)
		{
			this.db = db;
		}

		public void Add(Comment comment)
		{
			db.Comments.Add(comment);
		}

		public void Update() { }

		public void Delete(Comment comment)
		{
			db.Comments.Remove(comment);
		}

		public Comment GetCommentbyId(int userID, int postID, int commentID)
		{
			var comment = db.Comments.Find(userID, postID, commentID);

			return comment;
		}

		public Comment CreateComment(int userID, int postID, string text)
		{
			PostService pService = new PostService(db);
			UserService uService = new UserService(db);

			var time = DateTime.Now;
			Post post = pService.GetPostbyId(postID, userID);
			User user = uService.GetUserbyId(2);
			
			Comment newComment = new Comment{ User = user, Post = post, PostID = postID, UserID = 2, Text = text, Time = time };

			return newComment;
		}

		public void Save()
		{
			db.SaveChanges();
		}

	}
}