﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Posting.DataAccesLayer;
using Posting.Models;
using System.Data.Entity;

namespace PostingService.Services
{
	public class UserService
	{
		private PostingContext db;

		public UserService(PostingContext db) {
			this.db = db;
		}

		public void AddUser(User user)
		{
			db.Users.Add(user);
		}

		public void UpdateUser(User user)
		{
			throw new NotImplementedException();
		}

		public void DeleteUser(User user)
		{
			db.Users.Remove(user);
		}

		public User GetUserbyId(int userID)
		{
			return db.Users.Find(userID);
		}

		public ICollection<User> GetUserFriends(int userID)
		{
			var user = db.Users.Find(userID);
			var userFriends = user.Friends;

			return userFriends;
		}


		public ICollection<Post> GetUserPosts(int userID)
		{
			var posts = db.Posts.Where(p => p.UserID==userID);

			return posts.ToList();
		}

		public Boolean CanUserCommentReaction(int userID, int userOwnerPostID)
		{
			var user = db.Users.Find(userID);

			var can = user.Friends.Any(p => p.UserID == userOwnerPostID);

			return can;
		}

		public User GetUser(User userInfo)
		{
			var user = db.Users.Where(p => p.Email == userInfo.Email && p.Password == userInfo.Password).FirstOrDefault();

			return user;
		}

		public void Save()
		{
			db.SaveChanges();
		}


	}
}