﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Posting.DataAccesLayer;
using Posting.Models;

namespace PostingService.Services
{
	public class PostService
	{
		private PostingContext db;

		public PostService(PostingContext db)
		{
			this.db = db;
		}
		public void Add(Post post)
		{
			db.Posts.Add(post);
		}

		public void Update(Post post)
		{
		}

		public void Delete(Post post)
		{
			db.Posts.Remove(post);
		}

		public ICollection<Post> GetAllPosts()
		{
			return db.Posts.ToList();
		}

		public Post GetPostbyId(int postID, int userID)
		{
			var post = db.Posts.Where(p => p.UserID == userID && p.PostID == postID).FirstOrDefault();

			return post;
		}

		public ICollection<Comment> GetPostComments(int postID, int userID)
		{
			var post = db.Posts.Find(userID, postID);
			return post.Comments;
		}

		public ICollection<Reaction> GetPostReactions(int postID, int userID)
		{
			var post = db.Posts.Find(userID, postID);
			return post.Reactions;
		}

		public void Save()
		{
			db.SaveChanges();
		}

	}
}